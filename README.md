Split 4DN format
================

This is a simple web-based tool that take a "multiplexed" 4DN SPT file (several acquisitions per file) and generates a list of files with only one acquisition/cell per file. Such conversion is required by some SPT analysis tools (including [Spot-On](https://spoton.berkeley.edu).


* Use the [online version](https://tjian-darzacq-lab.gitlab.io/Split_4DN_format/),
* Read the [specifications](https://gitlab.com/tjian-darzacq-lab/Write_4DN_SPT_format_Matlab/4DN_SPT_format_PDF_description.pdf) of the format.
