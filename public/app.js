// Inspired from: https://stackoverflow.com/a/19109621/9734607
// Modified by MW, Jun 2018

var UploadController = function ($scope, fileReader) {
    console.log(fileReader);
    $scope.getFile = function () {
        $scope.progress = 0;
        fileReader.readAsText($scope.file, $scope)
                      .then(function(result) {
                          $scope.imageSrc = result;
			  $scope.SPTlines = result.split('\n');
			  $scope.SPTheader = $scope.SPTlines.filter(function(l) {return l.startsWith("#")});
			  $scope.SPTbody = $scope.SPTlines.filter(function(l) {return !l.startsWith("#")});

			  cn = getColumnIdx($scope.SPTheader[$scope.SPTheader.length-1]);
			  dat = {};
			  $scope.SPTbody.forEach(function(l) {
			      k = getKey(l, cn);
			      if (k in dat) {
				  dat[k].push(l);
			      } else if (k!="undefined_undefined") {
				  dat[k] = [l];
			      }
			  });
			  $scope.dat = dat;
                      });
    };

    getKey = function(l, cn) {
	ci = cn[0];
	bri = cn[1];
	d = l.split('\t');
	return d[ci]+'_'+d[bri];
    };
    getColumnIdx = function(l) {
	if (!l.startsWith("#Columns:")) {
	    alert("The last column of the header should begin with '#Columns:' and mention the names of the columns of the tab-separated block");
	    return;
	}
	ll = l
	cn = ll.substring(9).trim().split('\t').map(function(el){
	    return el.trim();
	});
	ci = cn.indexOf("Cell_ID");
	bri = cn.indexOf("BiologicalReplicate_ID");
	if (ci==-1|bri==-1) {
	    alert("The file does not contain both a 'Cell_ID' and a 'BiologicalReplicate_ID' column.");
	    return;
	}
	return [ci, bri];
    };

    $scope.getNewFile = function(k, bn) {
	fn = k+"_"+bn

	var csvString = $scope.SPTheader.join("\n") + "\n";
	csvString = csvString + $scope.dat[k].join("\n");
	csvString=escape(csvString);
	console.log(csvString);
	var a         = document.createElement('a');
	a.href        = 'data:attachment/tsv,' + csvString;
	a.target      = '_blank';
	a.download    = fn;

	document.body.appendChild(a);
	a.click();
    };
    
    $scope.$on("fileProgress", function(e, progress) {
        $scope.progress = progress.loaded / progress.total;
    });
 
};

app.directive("ngFileSelect",function(){

  return {
    link: function($scope,el){
      
      el.bind("change", function(e){
      
        $scope.file = (e.srcElement || e.target).files[0];
        $scope.getFile();
      })
      
    }
    
  }
  
  
})
